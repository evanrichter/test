{
  outputs = { nixpkgs, ... }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
    lib = pkgs.lib;
  in {
    packages.x86_64-linux.default = pkgs.testers.runNixOSTest {
      name = "foo";
      skipTypeCheck = true;
      nodes.m = { pkgs, ... }: {
        environment.systemPackages = [ pkgs.hello ];
        virtualisation.memorySize = 128;
        virtualisation.qemu.options = [
          "-fw_cfg flag,file=/flag"
        ];
      };
      testScript = ''
        m.start()
        m.wait_for_unit("default.target")
        m.execute("cat /sys/firmware/qemu_fw_cfg/by_name/flag/raw")
        m.succeed("cat /sys/firmware/qemu_fw_cfg/by_name/flag/raw")
      '';
    };
  };

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable-small";
  };
}
